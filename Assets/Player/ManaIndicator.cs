﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ManaIndicator : MonoBehaviour {

    Slider slider;
    Player player;

	// Use this for initialization
	void Start () {
        slider = GetComponent<Slider>();
        player = FindObjectOfType<Player>();
	}
	
	// Update is called once per frame
	void Update () {
        slider.maxValue = player.MaximumManaLevel;
        slider.value = player.energyLevel;
	}
}
