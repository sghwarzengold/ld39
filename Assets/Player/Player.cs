﻿using UnityEngine;

using Assets;
using Utils.Extensions;
using System;
using System.Collections;

public class Player : MonoBehaviour {

    Camera mainCamera;

    [SerializeField]
    float movementSpeed = 1f;

    [SerializeField]
    float jumpTime = 4f;

    [SerializeField]
    float jumpBoost = 4f;

    [SerializeField]
    float jumpEnergyCost = 10f;

    [SerializeField]
    float shieldSpeedPenalty = 2f;

    [SerializeField]
    float shieldPerSecondCost = 10f;

    public float energyLevel = 100f;

    [SerializeField]
    float manaRegenerationSpeed = 10f;

    public float MaximumManaLevel = 100f;

    public GameObject Shield;

    void Awake()
    {
        mainCamera = Camera.main;
    }

    void Start()
    {
        Shield.SetActive(false);
    }

    bool jump = false;
    float time = 0;
    Vector2 jumpDirection;
	// Update is called once per frame
	void Update () {

        if (energyLevel < MaximumManaLevel)
        {
            energyLevel = Math.Min(MaximumManaLevel, energyLevel + manaRegenerationSpeed* Time.deltaTime);
        }

        //Look at mouse
        var mousePosition = Input.mousePosition.ScreenToWorldPoint();
        var localPosition = transform.position;

        var dir = mousePosition - localPosition;
        var angle = Mathf.Atan2(dir.z, dir.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.down);

        if (jump)
        {
            if (time < jumpTime)
            {
                time += Time.deltaTime;
                var speed = Time.deltaTime * movementSpeed * jumpBoost;
                transform.localPosition = new Vector3(
                    transform.localPosition.x + speed * jumpDirection.x,
                    transform.localPosition.y,
                    transform.localPosition.z + speed * jumpDirection.y);
                return;

            }
            else
            {
                jump = false;
            }
        }

        if (Input.GetMouseButton(1))
        {
            if (!Shield.activeSelf)
                EnableShield();

            energyLevel -= shieldPerSecondCost * Time.deltaTime;
        }
        else
        {
            if (Shield.activeSelf)
                DisableShield();
        }

        //Moving
        var vertical = Input.GetAxis(Constants.VerticalAxis);
        var horizontal = Input.GetAxis(Constants.HorizontalAxis);

        jump = Input.GetKeyDown(KeyCode.Space);

        if (jump && energyLevel > jumpEnergyCost)
        {
            if (Shield.activeSelf)
            {
                DisableShield();
            }

            energyLevel -= jumpEnergyCost;

            time = 0;
            jump = true;
            jumpDirection = new Vector2(horizontal, vertical);
            return;
        }

        transform.localPosition = new Vector3(transform.localPosition.x + Time.deltaTime * movementSpeed * horizontal,
                                              transform.localPosition.y,
                                              transform.localPosition.z + Time.deltaTime * movementSpeed * vertical);
	}

    public void Bite(float damage)
    {
        energyLevel -= damage;
        if (energyLevel <= 0)
            GameController.GameOver();
    }

    public void AddEnergy(float amount)
    {
        energyLevel += amount;
    }

    void EnableShield()
    {
        movementSpeed /= shieldSpeedPenalty;
        Shield.SetActive(true);
    }

    void DisableShield()
    {
        Shield.SetActive(false);
        movementSpeed *= shieldSpeedPenalty;
    }
}
