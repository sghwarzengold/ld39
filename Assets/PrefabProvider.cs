﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrefabProvider : MonoBehaviour
{

    public EnergyBoost energyBoost;

    public static PrefabProvider Instance;

    private void Awake()
    {
        Instance = this;
    }
}
