﻿using UnityEngine;

namespace Utils.Extensions
{
    public static class Vector3Extension
    {
        public static Vector3 ScreenToWorldPoint(this Vector3 vector)
        {
            return Camera.main.ScreenToWorldPoint(vector);
        }

        public static Vector3 Multiply(this Vector3 v1, Vector3 v2)
        {
            return new Vector3(v1.y * v2.z - v1.z * v2.y,
                               v1.z* v2.x - v1.x * v2.z,
                               v1.x * v2.y - v1.y * v2.x);
        }

    }
}
