﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowToThePlayer : MonoBehaviour {

    Player player;
	// Use this for initialization
	void Start () {
        player = FindObjectOfType<Player>();
	}
	
	// Update is called once per frame
	void Update () {
        var dir = player.transform.position - transform.position;
        
        var cp = transform.position;
        var dt = Time.deltaTime;
        transform.position = new Vector3(cp.x + dir.x * dt, cp.y, cp.z + dir.z * dt);
        //transform.position = new Vector3(player.transform.position.x, cp.y, player.transform.position.z);
    }
}
