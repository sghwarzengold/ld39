﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {

    public Canvas GM;
    public Canvas WIN;


    void CallRestart()
    {
        Restart();
    }

    void CallWin()
    {
        SceneManager.LoadScene("menu");
    }

    public static void GameOver()
    {
        var t = FindObjectOfType<GameController>();
        t.GM.gameObject.SetActive(true);
        t.Invoke("CallRestart", 3);
    }

    internal static void Win()
    {
        var t = FindObjectOfType<GameController>();
        t.WIN.gameObject.SetActive(true);
        t.Invoke("CallWin", 3);
    }

    public static void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
