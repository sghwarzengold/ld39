﻿using UnityEngine;

public class Spawner : MonoBehaviour {

    Player player;

    public Enemy enemyPrefab;

    public bool autoSpawn;
    public float spawnTime;

    float timeBeforeSpawn;

    public float ActivationDistance = 15f;

	// Use this for initialization
	void Start ()
    {
        var sr = GetComponent<SpriteRenderer>();
        sr.enabled = false;
        timeBeforeSpawn = spawnTime;
        player = FindObjectOfType<Player>();
	}

    void Update()
    {
        if (!autoSpawn)
            return;

        if ((player.transform.position - transform.position).magnitude > ActivationDistance)
            return;

        timeBeforeSpawn -= Time.deltaTime;

        if (timeBeforeSpawn < 0)
        {
            Spawn();
            timeBeforeSpawn = spawnTime;
        }
    }

    public void Spawn()
    {
        var pos = new Vector3(transform.position.x, 0, transform.position.z);
        var newEnemy = Instantiate(enemyPrefab, pos, Quaternion.identity);
    }
}
