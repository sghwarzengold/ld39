﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnergyBoost : MonoBehaviour {

    public float Capacity = 10f;

    public float decreasingSpeed = 2f;

	void Update ()
    {
        Capacity -= decreasingSpeed;

        transform.localScale = new Vector3(Capacity / 10f, 1f, Capacity / 10f);

        if (Capacity <= 0)
        {
            Destroy(gameObject);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            other.gameObject.GetComponent<Player>().AddEnergy(Capacity);
            Destroy(gameObject);
        }
    }
}
