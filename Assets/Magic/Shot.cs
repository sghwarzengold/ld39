﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shot : MonoBehaviour
{

    public float Cost = 0f;
    public float Damage = 0f;

    public float movementSpeed = 5f;
    public float livingTime = 0.5f;

    CircleCollider2D circleCollider;

    // Use this for initialization
    void Start()
    {
        circleCollider = GetComponent<CircleCollider2D>();
    }

    void Update()
    {
        var scale = 0.1f + Damage / 5f;
        transform.localScale = new Vector3(scale, scale, scale);
    }

    internal void Fire(Vector3 dir)
    {
        StartCoroutine(FireCoroutine(dir));
    }

    private IEnumerator FireCoroutine(Vector3 dir)
    {
        var time = 0f;
        dir.y = 0;
        var norm = dir.normalized;
        while (time < livingTime)
        {
            time += Time.deltaTime;

            transform.localPosition = new Vector3(
                transform.localPosition.x + Time.deltaTime * movementSpeed * norm.x,
                transform.localPosition.y,
                transform.localPosition.z + Time.deltaTime * movementSpeed * norm.z);
            yield return null;
        }

        Destroy(gameObject);
    }
    private void OnTriggerEnter(Collider collision)
    {
        //if (collision.gameObject.tag == "enemy")
        //{
        //    Destroy(gameObject);
        //}
    }
}
