﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils.Extensions;

public class ShotMaker : MonoBehaviour {

    public Shot shotPrefab;

    Shot currentShot = null;
    Player player;

    public float costSpeed = 1f;
    public float damageSpeed = 2f;

    void Start()
    {
        player = FindObjectOfType<Player>();          
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            if (currentShot == null)
            {
                currentShot = Instantiate(shotPrefab, this.transform);
            }
            else
            {
                var delta = Time.deltaTime;
                currentShot.Cost += costSpeed * delta;
                currentShot.Damage += damageSpeed * delta;

                if (player.energyLevel < currentShot.Cost)
                {
                    MakeShot();
                }
            }
        }

        if (Input.GetMouseButtonUp(0))
        {
            MakeShot();
        }
	}

    void MakeShot()
    {
        if (currentShot == null)
            return;

        player.energyLevel -= currentShot.Cost;

        var mousePosition = Input.mousePosition.ScreenToWorldPoint();
        var localPosition = transform.position;

        var dir = mousePosition - localPosition;
        currentShot.Fire(dir);
        currentShot.transform.SetParent(null, true);

        currentShot = null;
    }
}
