﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealthIndicator : MonoBehaviour
{

    float initialHealth;
    Enemy enemy;

    void Start()
    {
        enemy = GetComponentInParent<Enemy>();
        initialHealth = enemy.Health;
    }

    void Update()
    {
        transform.localScale = new Vector3(enemy.Health / initialHealth, 0.15f);
    }
}
