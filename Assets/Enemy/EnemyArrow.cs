﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyArrow : MonoBehaviour
{
    public float livingTime = 2f;

    public float movementSpeed = 5f;

    public float Damage = 10f;

    Vector3 direction;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    private IEnumerator FireCoroutine()
    {
        var time = 0f;

        while (time < livingTime)
        {
            time += Time.deltaTime;
            var norm = direction.normalized;
            transform.localPosition = new Vector3(
                transform.localPosition.x + Time.deltaTime * movementSpeed * norm.x,
                transform.localPosition.y,
                transform.localPosition.z + Time.deltaTime * movementSpeed * norm.z);

            yield return null;
        }

        Destroy(gameObject);
    }
    private void OnTriggerEnter(Collider collision)
    {
        ProcessCollision(collision);

    }

    private void ProcessCollision(Collider collider)
    {
        var tag = collider.gameObject.tag;
        switch (tag)
        {
            case "Player":
                FindObjectOfType<Player>().Bite(Damage);
                Destroy(gameObject);
                break;
            case "enemy":
                Destroy(gameObject);
                break;
            case "shield":
                direction = new Vector3(direction.x * -1, direction.y, direction.z * -1);
                break;
            default:
                break;

        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        ProcessCollision(collision.collider);
    }
    internal void Release(Vector3 dir)
    {
        var angle = Mathf.Atan(dir.z / dir.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.down);
        direction = dir;
        StartCoroutine(FireCoroutine());
    }
}
