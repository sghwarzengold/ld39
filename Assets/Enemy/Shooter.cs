﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Utils.Extensions;

public class Shooter : Enemy
{
    public EnemyArrow arrowPrefab;

    public float secPerShot = 5f;

    float timeSinceLastShot = 0;

    protected override void CustomStart()
    {
        timeSinceLastShot = UnityEngine.Random.Range(0, secPerShot - 0.5f);
    }

    protected override void ApplyStun()
    {
        base.ApplyStun();
    }


    protected override void CustomUpdate()
    {
        if (agent.velocity == Vector3.zero)
        {
            var direction = player.transform.position - transform.position;

            var moveDistace = UnityEngine.Random.Range(1f, 5f);

            if (direction.magnitude > 10f)
            {
                agent.SetDestination(transform.position + direction.normalized * moveDistace * 1.5f);
            }
            else
            {

                var opposite = UnityEngine.Random.Range(-1f, 1f) > 0 ? Vector3.up : Vector3.down;
                var mul = direction.Multiply(opposite).normalized;

                agent.SetDestination(transform.position + mul * moveDistace);
            }
        }

        timeSinceLastShot += Time.deltaTime;
        if (timeSinceLastShot >= secPerShot)
        {
            timeSinceLastShot = 0;
            Shoot();
        }
    }

    private void Shoot()
    {
        var arrow = Instantiate(arrowPrefab);
        var direction = player.transform.position - transform.position;
        var safeDistance = 0.8f;
        arrow.transform.position = transform.position + safeDistance * direction.normalized;

        arrow.Release(direction);
    }
}
