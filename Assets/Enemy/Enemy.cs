﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{

    public float Health = 30f;

    public float Speed = 6f;

    public float Damage = 5f;

    public float attackSpeedBoost = 2f;

    public float attackDistance = 1f;

    protected Player player;

    Collider2D boxCollider;

    protected NavMeshAgent agent;

    public float ActiveDistance = 40f;

    // Use this for initialization
    void Start()
    {
        player = FindObjectOfType<Player>();
        boxCollider = GetComponent<Collider2D>();
        agent = GetComponent<NavMeshAgent>();

        agent.speed = Speed;

        CustomStart();
    }

    protected virtual void CustomStart()
    {
    }

    Vector3 prevDirection = Vector3.zero;

    bool isStunned = false;

    void Update()
    {
        if (Health <= 0)
        {
            Died();
            Destroy(gameObject);
            return;
        }

        if ((player.transform.position - transform.position).magnitude > ActiveDistance)
        {
            agent.velocity = Vector3.zero;
            return;
        }

        if (isStunned)
            return;

        CustomUpdate();
    }

    private void Died()
    {
        if (UnityEngine.Random.Range(0f, 1f) > 0.7)
        {
            var boost = Instantiate(PrefabProvider.Instance.energyBoost);
            boost.transform.position = transform.position;
        }
    }

    protected virtual void CustomUpdate()
    {
        agent.SetDestination(player.transform.position);
    }

    void OnTriggerEnter(Collider collision)
    {
        var tag = collision.gameObject.tag;
        ProcessCollision(collision, tag);
    }
    void OnCollisionEnter(Collision collision)
    {
        var tag = collision.gameObject.tag;
        ProcessCollision(collision.collider, tag);
    }

    private void OnCollisionStay(Collision collision)
    {
        var tag = collision.gameObject.tag;
        ProcessCollision(collision.collider, tag);
    }

    protected virtual void ProcessCollision(Collider collision, string tag)
    {
        switch (tag)
        {
            case "shot":
                Shot shot = collision.gameObject.GetComponent<Shot>();
                Health -= shot.Damage;
                break;
            case "arrow":
                EnemyArrow arrow = collision.gameObject.GetComponent<EnemyArrow>();
                Health -= arrow.Damage;
                break;
            case "Player":
                {
                    player.Bite(Damage * Time.deltaTime);
                    break;
                }
            case "shield":
                {
                    StartCoroutine(Stun());
                    break;
                }
            default:
                break;
        }
    }

    private IEnumerator Stun()
    {
        isStunned = true;
        ApplyStun();
        yield return new WaitForSeconds(1f);
        isStunned = false;
    }

    protected virtual void ApplyStun()
    {
        agent.ResetPath();
        agent.velocity = Vector3.zero;
    }
}
