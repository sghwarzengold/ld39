﻿namespace Assets
{
    static class Constants
    {
        public static string HorizontalAxis { get { return "Horizontal"; } }

        public static string VerticalAxis { get { return "Vertical"; } }
    }
}
